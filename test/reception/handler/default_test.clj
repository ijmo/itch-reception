(ns reception.handler.default-test
  (:require [clojure.test :refer :all]
            [integrant.core :as ig]
            [ring.mock.request :as mock]
            [reception.handler.default :as default]))

(deftest smoke-test
  (testing "example page exists"
    (let [handler  (ig/init-key :reception.handler.default/health {})
          response (handler (mock/request :get "/health"))]
      (is (= :ataraxy.response/ok (first response)) {:status "UP"}))))
