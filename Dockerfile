# Start with a base image containing Java runtime
FROM openjdk:8-jdk-alpine

RUN apk add --update tzdata

# Add Maintainer Info
LABEL maintainer="ijmo"

# Add a volume pointing to /tmp
# VOLUME /tmp

# Make port 8920 available to the world outside this container
EXPOSE 8920

# The application's jar file
ARG JAR_FILE=target/reception-0.1.0-SNAPSHOT-standalone.jar

# Add the application's jar to the container
ADD ${JAR_FILE} itch-reception.jar

ENV PORT=8920

# Run the jar file 
ENTRYPOINT ["java","-jar","/itch-reception.jar"]
