(ns reception.boundary.survey
  (:require [clojure.data.json :as json]
            [duct.database.mongodb.monger]
            [monger.collection :as mc]
            [langohr.channel   :as lch]
            [langohr.queue     :as lq]
            [langohr.consumers :as lc]
            [langohr.basic     :as lb]))


(def ^:const coll "survey")


(defn send-completed [{:keys [db]} mq user-id inst-id survey-id]
  (let [query {:surveyId survey-id}
        found (mc/find-one-as-map db coll query)
        files-map (select-keys found [:files :brFiles :psFiles :micFiles])]
    (when (not-empty files-map)
      (let [ch (lch/open mq)]
        (lb/publish ch
                    "ml.direct"
                    ""
                    (json/write-str (merge {:userId (Long/parseLong user-id)
                                            :instId inst-id
                                            :surveyId survey-id}
                                           files-map))
                    {:content-type "application/json"})))))


(defprotocol Survey
  (update-survey [db user-id inst-id survey-id entries])
  (append-file [db user-id inst-id survey-id user-key dir-path filename kind]))


(extend-protocol Survey
  duct.database.mongodb.monger.Boundary

  (update-survey
    [{:keys [db]} user-id inst-id survey-id entries]
    (let [query {:surveyId survey-id}
          found (mc/find-one-as-map db coll query)
          doc (merge query {:userId (Long/parseLong user-id) :instId inst-id} found entries)]
      (mc/update db "survey" query doc {:upsert true})))

  (append-file
    [{:keys [db]} user-id inst-id survey-id user-key dir-path filename kind]
     (let [query {:surveyId survey-id}
           found (mc/find-one-as-map db coll query)
           key-files (case kind
                       "META_MOTION_R" :brFiles
                       "PHONE_SENSOR" :psFiles
                       "PHONE_MIC" :micFiles
                       :files)
           files (if (and found (key-files found))
                   (conj (key-files found) filename)
                   [filename])
           doc (merge query found {:userKey user-key} {key-files files})]
       (mc/update db "survey" query doc {:upsert true}))))
