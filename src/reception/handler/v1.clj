(ns reception.handler.v1
  (:require [ataraxy.core :as ataraxy]
            [ataraxy.response :as response] 
            [integrant.core :as ig]
            [duct.logger :as logger]
            [reception.boundary.survey :refer [update-survey
                                               append-file
                                               ;; append-file2
                                               send-completed]]))


(defmethod ig/init-key ::survey-answer [_ {:keys [db logger]}]
  (fn [{[_ user-id inst-id survey-id entries] :ataraxy/result}]
    (update-survey db user-id inst-id survey-id entries)
    (logger/log logger :info ::survey-answer {:user-id user-id
                                              :inst-id inst-id
                                              :survey-id survey-id
                                              :entries entries})
    [::response/ok {:surveyId survey-id}]))


(defmethod ig/init-key ::file-metadata [_ {:keys [db logger]}]
  (fn [{[_ user-id inst-id survey-id user-key dir-path filename kind] :ataraxy/result}]
    (append-file db user-id inst-id survey-id user-key dir-path filename kind)
    (logger/log logger :info ::file-metadata {:user-id user-id
                                              :inst-id inst-id
                                              :survey-id survey-id
                                              :user-key user-key
                                              :dir-path dir-path
                                              :filename filename
                                              :kind kind})
    [::response/ok {:success "true"}]))


(defmethod ig/init-key ::file-tx-complete [_ {:keys [db mq logger]}]
  (fn [{[_ user-id inst-id survey-id] :ataraxy/result}]
    (send-completed db mq user-id inst-id survey-id)
    (logger/log logger :info ::file-tx-complete {:user-id user-id
                                                 :inst-id inst-id
                                                 :survey-id survey-id})
    [::response/ok {:success "true"}]))
